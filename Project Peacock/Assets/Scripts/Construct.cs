﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Construct : MonoBehaviour
{
    private bool transparent;
    private MeshRenderer mesh;
    [SerializeField]
    private Material standardMaterial;
    [SerializeField]
    private Material transparencyMaterial;

	// Use this for initialization
	void Start () {
        mesh = GetComponent<MeshRenderer>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        transparencyChange();
    }

    private void transparencyChange()
    {
        if (transparent)
        {
            if (transparencyMaterial != null)
            {
                mesh.material = transparencyMaterial;
            }
            else
            {
                transparent = false;
            }
        }
        else
        {
            if (standardMaterial != null)
            {
                mesh.material = standardMaterial;
            }
        }
    }


    public void changeTransparent(bool t)
    {
        transparent = t;
    }
    
}
