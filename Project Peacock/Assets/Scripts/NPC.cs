﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class NPC : MonoBehaviour
{
    [SerializeField]
    public string textDirectory;
    public string textFilename { get; set; }

    private string[] npcLines;

    private void Start()
    {
        textFilename = "0";
    }

    public string[] getNpcLines()
    {
        try
        {
            npcLines = File.ReadAllLines("Assets/Conversations/" + textDirectory + "/" + textFilename + ".txt", Encoding.UTF8);
        }
        catch (FileNotFoundException)
        {
            print("404 FileNotFound"+ "Assets/Conversations/" + textDirectory + "/" + textFilename + ".txt");
        }
            return npcLines;
    }
    

}
