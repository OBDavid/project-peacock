﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {

    [SerializeField]
    private KeyCode up = KeyCode.W;
    [SerializeField]
    private KeyCode left = KeyCode.A;
    [SerializeField]
    private KeyCode down = KeyCode.S;
    [SerializeField]
    private KeyCode right = KeyCode.D;
    [SerializeField]
    private KeyCode sprint = KeyCode.LeftShift;

    [SerializeField]
    private Sprite forward0, forward1, forward2, forward3, sideward0, sideward1, sideward2, sideward3, backward0, backward1, backward2, backward3;
    private SpriteRenderer sprite;
    private int runCircle;

    private long upTime, downTime, leftTime, rightTime;
    private string facing;
    private int cdMovement;
    private bool sprintOn;
    private bool collidedUp, collidedDown, collidedLeft, collidedRight;

    public bool movementAllowed { get; set; }

    // Use this for initialization
    void Start ()
    {
        facing = "up";
        movementAllowed = true;
        sprite = gameObject.GetComponentInChildren<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKey(up))
        {
            upTime++;
        }
        else
        {
            upTime = 0;
        }
        if (Input.GetKey(down))
        {
            downTime++;
        }
        else
        {
            downTime = 0;
        }
        if (Input.GetKey(left))
        {
            leftTime++;
        }
        else
        {
            leftTime = 0;
        }
        if (Input.GetKey(right))
        {
            rightTime++;
        }
        else
        {
            rightTime = 0;
        }
        if(Input.GetKey(sprint))
        {
            sprintOn = true;
        }
        else
        {
            sprintOn = false;
        }
    }

    void FixedUpdate()
    {
        if ((transform.position.x - 0.5) % 1 != 0 || (transform.position.z - 0.5) % 1 != 0) //Moved den Spieler solange bis er auf einem ganzen Feld ist
        {
            switch (facing) 
            {
                case "up":
                    moveUp();
                    break;
                case "down":
                    moveDown();
                    break;
                case "left":
                    moveLeft();
                    break;
                case "right":
                    moveRigth();
                    break;
            }
        }
        else
        if(cdMovement<=0&&movementAllowed) //prüft nach cooldown aufs Movement und ob das moven von irgendwo gesperrt wird
        {
            if (upTime > 0 || downTime > 0 || leftTime > 0 || rightTime > 0)
            {
                if (upTime != 0 && (downTime == 0 || upTime < downTime) && (leftTime == 0 || upTime < leftTime) && (rightTime == 0 || upTime < rightTime)) //schaut welcher Button am frühsten gedrückt worden ist
                {
                    moveUp();
                }
                else
                if (downTime != 0 && (upTime == 0 || downTime < upTime) && (leftTime == 0 || downTime < leftTime) && (rightTime == 0 || downTime < rightTime)) //--||--
                {
                    moveDown();
                }
                else
                if (leftTime != 0 && (upTime == 0 || leftTime < upTime) && (downTime == 0 || leftTime < downTime) && (rightTime == 0 || leftTime < rightTime)) //--||--
                {
                    moveLeft();
                }
                else
                {
                    moveRigth();
                }
            }
        }
        else
        {
            cdMovement--;
        }
        runCircle--;
    }

    void moveUp()
    {
        if (!"up".Equals(facing))
        {
            sprite.flipX = false;
            sprite.sprite = backward0;
            runCircle = 39;
            // Sprite turnen
            cdMovement = 4;
        }
        else
        {
            if (!collidedUp)
            {
                if (sprintOn)
                {
                    if (((transform.position.z + 0.0625f) - .5f) % 1 == 0)
                    {
                        transform.Translate(new Vector3(0, 0, -0.0625f));
                    }
                    transform.Translate(new Vector3(0, 0, 0.125f));
                }
                else
                {
                    transform.Translate(new Vector3(0, 0, 0.0625f));
                }
                collidedUp=false;
                collidedDown = false;
                collidedLeft = false;
                collidedRight = false;

                if(runCircle<0)
                {
                    runCircle = 39;
                }
                switch(runCircle/10)
                {
                    case 3:
                        sprite.sprite = backward0;
                        break;
                    case 2:
                        sprite.sprite = backward1;
                        break;
                    case 1:
                        sprite.sprite = backward2;
                        break;
                    case 0:
                        sprite.sprite = backward3;
                        break;
                }
            }
            else
            {
                //bump
            }
        }
        facing = "up";
    }
    void moveDown()
    {
        if (!"down".Equals(facing))
        {
            sprite.sprite = forward0;
            sprite.flipX = false;
            runCircle = 39;
            // Sprite turnen
            cdMovement = 4;
        }
        else
        {
            if (!collidedDown)
            {
                if (sprintOn)
                {
                    if(((transform.position.z - 0.0625f)-.5f) % 1==0)
                    {
                        transform.Translate(new Vector3(0, 0, +0.0625f));
                    }
                    transform.Translate(new Vector3(0, 0, -0.125f));
                }
                else
                {
                    transform.Translate(new Vector3(0, 0, -0.0625f));
                }
                collidedUp = false;
                collidedDown = false;
                collidedLeft = false;
                collidedRight = false;

                if (runCircle < 0)
                {
                    runCircle = 39;
                }
                switch (runCircle / 10)
                {
                    case 3:
                        sprite.sprite = forward0;
                        break;
                    case 2:
                        sprite.sprite = forward1;
                        break;
                    case 1:
                        sprite.sprite = forward2;
                        break;
                    case 0:
                        sprite.sprite = forward3;
                        break;
                }
            }
            else
            {
                //bump
            }
        }
        facing = "down";
    }
    void moveLeft()
    {
        if (!"left".Equals(facing))
        {
            sprite.flipX = false;
            sprite.sprite = sideward0;
            cdMovement = 4;
            runCircle = 39;
        }
        else
        {
            if (!collidedLeft)
            {
                if (sprintOn)
                {
                    if (((transform.position.x - 0.0625f) - .5f) % 1 == 0)
                    {
                        transform.Translate(new Vector3(+0.0625f, 0, 0));
                    }
                    transform.Translate(new Vector3(-0.125f, 0, 0));
                }
                else
                {
                    transform.Translate(new Vector3(-0.0625f, 0, 0));
                }
                collidedUp = false;
                collidedDown = false;
                collidedLeft = false;
                collidedRight = false;

                if (runCircle < 0)
                {
                    runCircle = 39;
                }
                switch (runCircle / 10)
                {
                    case 3:
                        sprite.sprite = sideward0;
                        break;
                    case 2:
                        sprite.sprite = sideward1;
                        break;
                    case 1:
                        sprite.sprite = sideward2;
                        break;
                    case 0:
                        sprite.sprite = sideward3;
                        break;
                }
            }
            else
            {
                //bump
            }
        }
        facing = "left";
    }
    void moveRigth()
    { 
        if (!"right".Equals(facing))
        {
            sprite.flipX = true;
            sprite.sprite = sideward0;
            runCircle = 39;
            // Sprite turnen
            cdMovement = 4;
        }
        else
        {
            if (!collidedRight)
            {
                if (sprintOn)
                {
                    if (((transform.position.x + 0.0625f) - .5f) % 1 == 0) //fixt einen Bug
                    {
                        transform.Translate(new Vector3(-0.0625f, 0, 0));
                    }

                    transform.Translate(new Vector3(0.125f, 0, 0));
                }
                else
                {
                    transform.Translate(new Vector3(0.0625f, 0, 0));
                }
                collidedUp = false;
                collidedDown = false;
                collidedLeft = false;
                collidedRight = false;

                if (runCircle < 0)
                {
                    runCircle = 39;
                }
                switch (runCircle / 10)
                {
                    case 3:
                        sprite.sprite = sideward0;
                        break;
                    case 2:
                        sprite.sprite = sideward1;
                        break;
                    case 1:
                        sprite.sprite = sideward2;
                        break;
                    case 0:
                        sprite.sprite = sideward3;
                        break;
                }
            }
            else
            {
                //bump
            }
        }
        facing = "right";
    }

    private void OnTriggerStay(Collider coll) //Collision Detection  PS: Wände werden nur als Wand erkannt wenn sie auf der selben Höhe wie Spieler sind
    {
       
        if (coll.transform.position.y == transform.position.y)
        {
            List<float> cubesZ = new List<float>();
            int scaleZ = (int)((coll.transform.localScale.z-coll.transform.localScale.z % 2)/2);
            for (int i = scaleZ; i >= (1 - 1 * coll.transform.localScale.z % 2) - scaleZ; i--)
            {
                cubesZ.Add(coll.transform.position.z + i-0.5f+0.5f*(coll.transform.localScale.z % 2));
            }

            List<float> cubesX = new List<float>();
            int scaleX = (int)((coll.transform.localScale.x -coll.transform.localScale.x % 2) / 2);
            for (int i = scaleX; i >= (1 - 1 * coll.transform.localScale.x % 2) - scaleX; i--)
            {
                cubesX.Add(coll.transform.position.x + i - 0.5f + 0.5f * (coll.transform.localScale.x % 2));
            }


            if (cubesZ.Contains(transform.position.z -1) && cubesX.Contains(transform.position.x))
            {
                collidedDown = true;
            }
            if (cubesZ.Contains(transform.position.z + 1) && cubesX.Contains(transform.position.x))
            {
                collidedUp = true;
            }
            
            if (cubesX.Contains(transform.position.x + 1) && cubesZ.Contains(transform.position.z))
            {
                collidedRight = true;
            }

            if (cubesX.Contains(transform.position.x - 1) && cubesZ.Contains(transform.position.z))
            {
                collidedLeft = true;
            }
        }
    }
    

    public string getFacing()
    {
        return facing;
    }
}
