﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SightBlock : MonoBehaviour {

    private Construct lastColl;
	// Use this for initialization
	void Start () {

        lastColl = null;
    }

    // Update is called once per frame
    void Update()
    {
        /*
         Debug.DrawRay(transform.position, new Vector3(0,-162,100), Color.green, 1f,true);
        Vector3 gesamt = transform.position + new Vector3(0, -162, 100);
        Debug.Log(gesamt.x + " " + gesamt.y + " " + gesamt.z);
		*/
        
        RaycastHit hit;
        Physics.Raycast(transform.position, new Vector3(0, -1.57f, 1), out hit, Mathf.Infinity);
        if (hit.collider != null && hit.collider.tag != "Player")
        {
            if (hit.collider.GetComponent<Construct>() != null)
            {
                //Debug.Log("WOW");
                lastColl = hit.collider.GetComponent<Construct>();
                lastColl.changeTransparent(true);
            }
            else
            {
                //Debug.Log(hit.collider.GetType());
                if (lastColl != null)
                {
                    lastColl.changeTransparent(false);
                }
            }
        }
    }
}
