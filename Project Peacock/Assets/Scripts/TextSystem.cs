﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class TextSystem : MonoBehaviour
{
    
    private Movement move;
    private NPC npc;

    [SerializeField]
    private KeyCode interactKey = KeyCode.Return;
    [SerializeField]
    private KeyCode escapeKey = KeyCode.Escape;
    private bool interactKeyPressed, escapeTextPressed;

    private GameObject textbox;
    private Text textOfTextbox;
    private int textCount;

    private enum ConversationStatus
    {
        Inactive,Active,Paused
    }
    private ConversationStatus status;

    [SerializeField]
    private Button option1, option2, option3, option4;
    [SerializeField]
    private GameObject panel;
    private int optionPicked;

    private int nextLineCD;
    private bool fixedUpdated;
    private bool autoskip;

    // Use this for initialization
    void Start()
    {
        status = ConversationStatus.Inactive;
        textbox = GameObject.FindGameObjectWithTag("Textbox");
        if (textbox != null)
        {
            textbox.gameObject.SetActive(false);
            textOfTextbox = textbox.GetComponentInChildren<Text>();


            panel.SetActive(false);

            option1.gameObject.gameObject.SetActive(false);
            option1.onClick.AddListener(delegate {
                optionPicked = 1;
                option1.gameObject.SetActive(false);
                option2.gameObject.SetActive(false);
                option3.gameObject.SetActive(false);
                option4.gameObject.SetActive(false);
                panel.SetActive(false);
                autoskip = true;
                status = ConversationStatus.Active;
            });
            option2.gameObject.SetActive(false);
            option2.onClick.AddListener(delegate {
                optionPicked = 2;
                option1.gameObject.SetActive(false);
                option2.gameObject.SetActive(false);
                option3.gameObject.SetActive(false);
                option4.gameObject.SetActive(false);
                panel.SetActive(false);
                autoskip = true;
                status = ConversationStatus.Active;
            });

            option3.gameObject.SetActive(false);
            option3.onClick.AddListener(delegate {
                optionPicked = 3;
                option1.gameObject.SetActive(false);
                option2.gameObject.SetActive(false);
                option3.gameObject.SetActive(false);
                option4.gameObject.SetActive(false);
                panel.SetActive(false);
                autoskip = true;
                status = ConversationStatus.Active;
            });

            option4.gameObject.SetActive(false);
            option4.onClick.AddListener(delegate {
                optionPicked = 4;
                option1.gameObject.SetActive(false);
                option2.gameObject.SetActive(false);
                option3.gameObject.SetActive(false);
                option4.gameObject.SetActive(false);
                panel.SetActive(false);
                autoskip = true;
                status = ConversationStatus.Active;
            });
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(fixedUpdated)
        {
            interactKeyPressed = false;
            escapeTextPressed = false;
            fixedUpdated = false;
        }
        interactKeyPressed = Input.GetKeyDown(interactKey) || interactKeyPressed;
        escapeTextPressed = Input.GetKeyDown(escapeKey) || escapeTextPressed;
    }

    void FixedUpdate()
    {
        if (optionPicked != 0 && npc != null)
        {
            npc.textFilename = npc.textFilename + optionPicked;
            print(npc.textFilename);
            optionPicked = 0;
            textCount = 0;
        }

        if (nextLineCD <= 0 && status==ConversationStatus.Active && (interactKeyPressed || autoskip || escapeTextPressed))
        {
            autoskip = false;
            speakLine(textCount);
        }
        fixedUpdated = true;
        nextLineCD--;
    }

    private void OnTriggerStay(Collider coll)
    {
        if (interactKeyPressed && status == ConversationStatus.Inactive)
        {
            move = gameObject.GetComponent<Movement>();
            if (move != null && nextLineCD <= 0)
            {

                if (coll.transform.position.x == transform.position.x && coll.transform.position.z - 1 == transform.position.z && move.getFacing().Equals("up")
                    || coll.transform.position.x == transform.position.x && coll.transform.position.z + 1 == transform.position.z && move.getFacing().Equals("down")
                    || coll.transform.position.z == transform.position.z && coll.transform.position.x + 1 == transform.position.x && move.getFacing().Equals("left")
                    || coll.transform.position.z == transform.position.z && coll.transform.position.x - 1 == transform.position.x && move.getFacing().Equals("right"))
                {                    
                    npc = coll.GetComponent<NPC>();
                    startSpeak();
                }
                else
                {
                    interactKeyPressed = false;
                }
            }
        }
        
    }

    private void startSpeak()
    {
        textCount = 0;
        if (npc.getNpcLines().Length > 0)
        {
            move.movementAllowed = false;
            status = ConversationStatus.Active;
            textbox.gameObject.SetActive(true);
            speakLine(0);
        }
    }

    private void stopSpeaking()
    {
        nextLineCD = 30;
        move.movementAllowed = true;
        status = ConversationStatus.Inactive;
        textbox.gameObject.SetActive(false);
    }

    private void speakLine(int i)
    {
        interactKeyPressed = false;
        escapeTextPressed = false;

        if (i < npc.getNpcLines().Length && npc.getNpcLines()[i] != null)
        {
            if (npc.getNpcLines()[i].Contains("|")) 
            {
                string[] answers = npc.getNpcLines()[i].Split('|'); //erstes Feld ist leer
                if (answers.Length-1 <= 4)
                {
                    status = ConversationStatus.Paused;
                    askQuestion(answers);
                }
            }
            else if (npc.getNpcLines()[i].StartsWith("/"))
            {
                print(npc.getNpcLines()[i]);
                textCount++;
                autoskip = true;
            }
            else
            {
                textOfTextbox.text = npc.getNpcLines()[i];
                textCount++;
                nextLineCD = 10;
            }
        }
        else
        {
            stopSpeaking();
        }
    }

    private void askQuestion(string[] answers)
    {
        for (int i = 1; i < answers.Length; i++)
        {
            switch (i)
            {
                case 1:
                    option1.gameObject.SetActive(true);
                    option1.GetComponentInChildren<Text>().text = answers[1];
                    break;
                case 2:
                    option2.gameObject.SetActive(true);
                    option2.GetComponentInChildren<Text>().text = answers[2];
                    break;
                case 3:
                    option3.gameObject.SetActive(true);
                    option3.GetComponentInChildren<Text>().text = answers[3];
                    break;
                case 4:
                    option4.gameObject.SetActive(true);
                    option4.GetComponentInChildren<Text>().text = answers[4];
                    break;
                default:
                    break;
            }
            panel.SetActive(true);
        }
    }

}
